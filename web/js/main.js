/*

https://dsteffens.cartodb.com/tables/current_listings_2802062015

*/

/*jshint multistr: true */
/*jshint -W030 */

var STREET_VIEW_API_KEY = 'AIzaSyCD4olywJlcMkBbFsWDS32PGPuZRkSaaAg',
  //,cartoTableName = 'current_listings_2802062015'
  //,cartoUserName = 'dsteffens'
  //,cartoTableName = 'export_combined_test_28revised_29',
  //cartoUserName = 'varik',
  cartoUserName = 'cardinalmaps',
  cartoSqlUrl = 'http://' + cartoUserName + '.cartodb.com/api/v2/sql/?&q=',
  googleStreetImageUrl =
  'http://maps.googleapis.com/maps/api/streetview?size=300x300&key=' +
  STREET_VIEW_API_KEY + '&location=',
  googleStreetUrl = 'https://www.google.com/maps?q&layer=c&cbll=',
  //,popupFields = "address_1,dbo_space_info_notes,lat,long,zoning,user_3,city,state,zip_code,image,occupancy,description,property_type,min_price,max_price,row_count",
  popupTemplate = Handlebars.compile($("#popup-template").html()),
  infoTemplate = Handlebars.compile($("#info-template").html()),
  searchResultsTemplate = Handlebars.compile($("#searchlist-template").html()),
  map,
  colors = {};

var selectPlace;

var makeMap = function(){

  L.mapbox.accessToken = 'pk.eyJ1IjoiZHN0ZWZmZW5zIiwiYSI6IjJaWVRSbFkifQ.J9tsaFnY0GgTZFdfAanUhQ';

  map = L.map('map', {
    zoomControl: false,
    center: [39.968807, -76.703269],
    zoom: 9
  });

  var streets = L.mapbox.tileLayer('mapbox.streets');
  var satellite = L.mapbox.tileLayer('mapbox.streets-satellite');

  //map.addLayer(satellite);
  map.addLayer(satellite);
  map.addLayer(streets);

  curLayer = 0;
  switchLayer = function(){
    $([streets, satellite][curLayer].getContainer()).hide();
    curLayer = 1*(!curLayer);
    $([streets, satellite][curLayer].getContainer()).show();
  }

  map.addControl(new mapControlSwitch('map-switch', { switch: switchLayer }));

  //map.addControl(new L.Control.Layers({'Satellite': satellite, "Streets": streets }, {}));

  map.addControl(L.mapbox.geocoderControl('mapbox.places', { position: 'topright', autocomplete: true }));

  new L.Control.Zoom({
    position: 'topright'
  }).addTo(map);

  addDrawControl(map);

  map.state = {};
  map.makeSQL = makeSQL;
  map.filterMarkers = filterMarkers;

  $('.reset-btn').click(resetMap);

  $('.sidebar .bothHide').hide();

  $('#map').on('click', '.view-listings-btn', function() {
    var i = +this.id.replace('viewListings-','');
    map.data[i].link && window.open(map.data[i].link);
  });
}

var makeColors = function(){
  $('#offer_type label').each(function() {
    var prop = $('input', this).attr('id').replace('offer_type-', '').replace(/_/g, ' '),
      color = $(this).css('color');
    //console.log(prop);
    colors[prop] = rgb2hex(color);
  });
  console.log(colors);
}

var resetMap = function() {
  $('.check-box input').prop('checked', false);
  $('.check-box').removeClass('checked');
  $('input[type=text]').val('');
  $('.right-sidebar .close').trigger('click');
  $('.search-hint').hide();
  //$('.right-sidebar').removeClass('visible');
  if (map._popup) map.closePopup();
  map.lastBoundsLayer && map.removeLayer(map.lastBoundsLayer);
  map.state = {};
  makeSQL(map.state);
}

var makeCartoLayer = function(){
  $.getJSON(cartoSqlUrl + sqlProperties, function(d) {
    map.data = d.rows;
    console.log(map.data);

    map.data.forEach(function(d){ d.map = map; });
    map.data.forEach(addMarker);

    searchResults(map.data);
  });
}

$(document).ready(function() {

  makeMap();

  initFiltersUI(map);

  makeColors();

  makeCartoLayer(map);

  initPropTypeNumbers();

  $('#expand-btn').trigger('click');

});

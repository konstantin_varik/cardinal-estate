var renderPopup = function(data) {
  //console.log(data);

  data.spaces = [];

  var suits = data.suits.split(',');
  var sfs = data.sfs.split(',');
  var is_retail = data.is_retail.split(',');
  var is_office = data.is_office.split(',');
  var is_industrial = data.is_industrial.split(',');
  var type = data.type == 'Commercial' ? 'Retail' : data.type;
  var types = [];

  data['for' + data.offer_type] = true;
  if (data.notes_)
    data.notes = data.notes_.formatNotes(50);

  return popupTemplate(data);

};

var popupImage = function (selector, url) {
	console.log(url);
	if (lastImageUrl != url) {
		$(selector).parent().hide();
		$('#loadingImage2').parent().show();
		$(selector).attr('src', url).on('load', function() {
			$(this).parent().show();
			$('#loadingImage2').parent().hide();
		});
		lastImageUrl = url;
	}
}

var addMarker = function(d, i){
  //if (!d.link) return;
  d.i = i;
  d.latlng = L.latLng([d.lat, d.long]);
  d.marker = L.marker(d.latlng, {
      icon: L.mapbox.marker.icon({
          'marker-size': 'small',
          //'marker-symbol': 'marker',
          'marker-color': colors[d.offer_type]
      })
  });
  d.marker.data = d;
  d.marker.addTo(map);

  d.marker.bindPopup(renderPopup(d));

  d.link && $('#viewListings-' + i).on('click', function() {
    console.log(d)
    window.open(d.link);
  });

}

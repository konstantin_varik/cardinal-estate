var searchResults = function(data) {
  var pageLength = 20;
    pages = [],
    curPage = 0;

  for(var i = 0; i < Math.ceil(data.length / pageLength); i++)
    pages.push({ num: i + 1 });

  //console.log(Math.floor(data.length / pageLength),pages);

  data.pages = pages;

  data.forEach(function(d, index) {
    var suits = d.suits.split(','),
      sfs = d.sfs.split(','),
      price = d.prices.split(','),
      rent_per_sf = d.rent_per_sfs.split(','),
      is_retail = d.is_retail.split(','),
      is_office = d.is_office.split(','),
      is_industrial = d.is_industrial.split(','),
      type = d.type == 'Commercial' ? 'Retail' : d.type;

    d.spaces = [];
    var notes = d.notes_.split('fs17')[1];
    if (notes) d.notes_ = notes;
    else {
      notes = d.notes_.split('fs20')[1];
      if (notes) d.notes_ = notes;
    }

    d.notes = d.notes_ ? d.notes_.formatNotes(100) : '';

    d.sids.split(',').forEach(function(sid, i) {
      var types = [];
      if (is_retail[i].toLowerCase() == 't') types.push(
        'Retail');
      if (is_office[i].toLowerCase() == 't') types.push(
        'Office');
      if (is_industrial[i].toLowerCase() == 't') types.push(
        'Industrial');
      d.spaces.push({
        sid: sid,
        property: d,
        suit: suits[i],
        suiteWord: suits[i].length > 7 ? '' : 'Suite',
        sf: +sfs[i] ? (+sfs[i]).formatMoney(0) : null,
        i: d.i,
        isFirst: !i,
        types: types.length ? types : [d.type],
        offerType: (d.is_for_lease ? 'Lease ' : '') + (d.is_for_sale ?
          'Sale ' : ''),
        price: +price[i] ? '$ ' + (+price[i]).formatMoney(0) : null,
        rent_per_sf: +rent_per_sf[i] ? '$ ' + (+rent_per_sf[
          i]).formatMoney(0) : null,
        //hidden: curPage != Math.floor(index / pages.length),
        pageIndex: Math.floor(index / pageLength)
      });

      d.numSpaces = d.spaces.length;

    });
  });
  //console.log(data);
  $('#searchResults').html(searchResultsTemplate(data));

  $('#searchResults .first-row').hide();
  selectPage(0);
  $('#searchResults .list-pagination span.page-num').click(function(e){
    console.log(e.target, e.target.id);
    selectPage(+e.target.id.replace('list-page-',''));
  });
  $('#searchResults .list-pagination span.page-left').click(function(){
    selectPage((curPage - 1 + pages.length) % pages.length);
  });
  $('#searchResults .list-pagination span.page-right').click(function(){
    selectPage((curPage + 1 + pages.length) % pages.length);
  });
  $('#searchResults .show-on-map').click(function() {
    var i = +this.id.replace('show-on-map-', ''),
      latlng = L.latLng(data[i].lat, data[i].long);
    $('html, body').animate({
      scrollTop: $("#map").offset().top
    }, 1000);
    map.setView(latlng, 16);
    console.log(this.id, i, data[i]);
    //renderPopup(data[i], latlng);
    data[i].marker.openPopup();
  });
  $('#searchResults .property-info-btn').click(function() {
    var i = +this.id.replace('property-info-btn-', '');
    data[i].link && window.open(data[i].link);
  });
  $('#searchResults .save-listing-wrap label').click(function() {
    var i = +this.id.replace('save-listing-','');
    //console.log(map.data[i]);
		var checkStatus = $('input', this).prop("checked");
    console.log(checkStatus);
		if (checkStatus) {
			$(this).addClass('checked');
      map.data[i].saved = true;
      $('#searchResults #description-' + i).addClass('saved');
		} else {
			$(this).removeClass('checked');
      map.data[i].saved = false;
      $('#searchResults #description-' + i).removeClass('saved');
		}
	});


  function selectPage(pageIndex){
    $('#searchResults div.page-index-' + curPage).hide();
    $('#searchResults span#list-page-' + curPage).removeClass('page-current');
    $('#searchResults div.page-index-' + pageIndex).show();
    $('#searchResults span#list-page-' + pageIndex).addClass('page-current');
    curPage = pageIndex;
  }
};

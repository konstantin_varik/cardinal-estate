var map;

function initFiltersUI(_map) {
  map = _map;

  if (window.screen.width < 900) {
    // resolution is below 10 x 7
    $('aside').remove();
  }

	//$(".sidebar .part").hide().eq(0).show();
	$(".part.base").show(500);
	$('.part.expand').hide(500);

	$('#expand-btn').click(function() {
		$(".part.base").hide(500);
		$('.part.expand').show(500);
	});
	$('#collapse-btn').click(function() {
		$(".part.base").show(500);
		$('.part.expand').hide(500);
	});
  var mobileAside = true;
  $('#mobile-collapse-btn').click(function() {
    if (mobileAside = !mobileAside){
		  $(".aside-mobile ").show(500);
      $('#mobile-collapse-btn').text('HIDE');
    }else{
      $(".aside-mobile ").hide(500);
      $('#mobile-collapse-btn').text('FILTERS');
    }
	});
  $('#mobile-collapse-btn').click();

	$('.check-box').on('click', function() {
		var checkStatus = $('input', this).prop("checked");
		if (checkStatus) {
			$(this).addClass('checked');
		} else {
			$(this).removeClass('checked');
		}
	});

	$('.location-form .submit-btn').on('click', function() {
		selectPlace();
	});

	$('.search-hint').hide();

	$('.search-box-btn,.search-circle-btn').click(function() {
    $('.search-hint').hide();
		map.lastBoundsLayer && map.removeLayer(map.lastBoundsLayer);
	});
	$('.search-box-btn').click(function() {
    $('.search-hint.rectangle-hint').show();
		new L.Draw.Rectangle(map, map.drawControl.options.rectangle).enable();
	});
	$('.search-circle-btn').click(function() {
    $('.search-hint.circle-hint').show();
		new L.Draw.Circle(map, map.drawControl.options.circle).enable();
    //circle.editing.enable();
	});

	$('input.leasePricePerSqFt').focus(function() {
    $('.leasePricePerMonth').addClass('disabled');
    $('.leasePricePerSqFt').removeClass('disabled');
    $('input.leasePricePerMonth').val('');
  });
  $('input.leasePricePerMonth').focus(function() {
    $('.leasePricePerSqFt').addClass('disabled');
    $('.leasePricePerMonth').removeClass('disabled');
    $('input.leasePricePerSqFt').val('');
  });

  $('.filterInput[type=text]').change(filter);
  $('.filterInput[type=text]').on('input',filter);
  $('.filterInput[type=checkbox]').click(filter);

}

var filter = function() {
  var state = map.state;
  if ($('.offer_type-sale input').is(':checked') == $('.offer_type-lease input').is(
      ':checked')) {
    state.price_min = null;
    state.size_min = null;
    $('input#price_min').val('');
    $('input#size_min').val('');
  }
  if ($('.offer_type-sale input').is(':checked') && !$('.offer_type-lease input').is(
      ':checked')) {
    $('.forSale').show();
    $('.sidebar .bothShow').hide();
    $('.sidebar .bothHide').show();
    $('.forLease').hide();
  } else if (!$('.offer_type-sale input').is(':checked') && $(
      '.offer_type-lease input').is(':checked')) {
    $('.sidebar .bothShow').hide();
    $('.sidebar .bothHide').show();
    $('.forSale').hide();
    $('.forLease').show();
  } else {
    state.price_min = null;
    state.size_min = null;
    $('input#price_min').val('');
    $('input#size_min').val('');
    $('.forSale').show();
    $('.forLease').show();
    $('.sidebar .bothShow').show();
    $('.sidebar .bothHide').hide();
  }

  var key_value = this.id.split('-');
  if (this.type == 'checkbox') {

    if (key_value[0] == 'offer_type') {
      if (this.checked) {
        state[key_value[1]] = true;
      } else
        delete state[key_value[1]];
    }
    if (key_value[0] == 'property_type') {
      var propName = this.id.replace('property_type-', '').replace('_',
        ' ');
      if (this.checked) {
        if (!state.property_type)
          state.property_type = {};
        state.property_type[propName] = true;
      } else
        delete state.property_type[propName];
      if (!_keys(state.property_type).length)
        delete state.property_type;
    }
    if (key_value[0] == 'price_min') {
      if (+this.value > 0) {
        state.price_min = +this.value;
      } else
        delete state.price_min;
    }
    if (key_value[0] == 'price_max') {
      if (+this.value > 0) {
        state.price_max = +this.value;
      } else
        delete state.price_max;
    }
  }

  if (this.type == 'text')
    state[key_value[0]] = this.value;

  //console.log(state, key_value, this.value);

  map.makeSQL(state);
};

filterMarkers = function(){
  this.data.forEach(function(d){
    if (d.hide) {
      $(d.marker._icon).hide();
    } else {
      $(d.marker._icon).show();
    }
  })
}

selectPlace = function(search, field) {
  console.log(field, search);

  var place,
    state = map.state;

  if (field) {
    map.lastBoundsLayer && map.removeLayer(map.lastBoundsLayer);
    delete map.state.circle;
    delete map.state.bounds;

    inputFields.forEach(function(f) {
      console.log('!!!	 ', f, field, f != field);
      if (f != field)
        $("input#" + f).val('');
    });

    state = {
      geosearch: {
        field: field,
        value: search.value
      }
    };


    if (field == 'zip_code_') {
      place = 'ZIP ' + search.zip_code + ', ' + zip_codes[search.zip_code]
        .city + ', ' + zip_codes[search.zip_code].state;
      $("input#city").val(zip_codes[search.zip_code].city);
      $("input#state").val(zip_codes[search.zip_code].state);
    }
    if (field == 'city_') {
      place = search.city + ', ' + cities[search.city].state;
      $("input#state").val(cities[search.city].state);
    }
    if (field == 'state_') {
      place = search.state;
    }
  } else {
    console.log($("input#city").val(), $("input#city").val(), $(
      "input#city").val());
    if (!$("input#city").val() && !$("input#city").val() && !$(
        "input#city").val()) {
      place = 'Everywhere';
      state.geosearch = null;
    }
  }
  //console.log(state);
  map.makeSQL(state);

  search && search.lat && map.setView([search.lat, search.lon], field ==
    'zip_code_' ? 13 : (field == 'city_' ? 11 : 9), {
      animate: true
    });

  $('.sidebar .place').html(place);

};

var initPropTypeNumbers = function(){
  var propTypes = [],
    propType;

  $('#property_types input').each(function() {
    //console.log(this.id, this.id.replace('property_type-','').replace(/_/g, ' '));
    propTypes.push(propType = this.id.replace('property_type-', '').replace(
      /_/g, ' '));

    var label = $(this.parentNode);

    $.getJSON(cartoSqlUrl + sqlPropType(propType), function(data) {
      //console.log(data);
      if (data) {
        var countByType = data.rows && data.rows[0] && data.rows[0]
          .count;
        label.html(label.html() + ' (' + (countByType || 0) + ')');
        if (!countByType) {
          $('input', label).prop('disabled', true);
          label.addClass('disabled');
        }
        $('input', label).change(filter);
      }
    });
  });
}

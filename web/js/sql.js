var
  //cartoTableName = 'rock4_',
  cartoTableName = 'rockwebexport_1',
  leaseOrSale = {
    l: 'lease___',
    s: 'sale___',
    ls: 'sale_or_lease___'
  },
  currentLS = 'ls',

  //export_table_1_of_2_property_property_key_ as pid ,\

  sqlPropertiesAgg = "select \
      property_key as pid ,\
      avg(lat) as lat ,\
      avg(long) as long ,\
      (array_agg(address_1||', '||city||', '||state||' '||zip_code))[1] as address ,\
      (array_agg(property_name))[1] as name ,\
      (array_agg(property_type))[1] as type ,\
      (array_agg(cartodb_id))[1] as id ,\
      (array_agg(loopnet_site))[1] as link ,\
      array_to_string(array_agg(property_type='Office'),',',' ') as is_office ,\
      array_to_string(array_agg(property_type='Industrial'),',',' ') as is_industrial ,\
      array_to_string(array_agg(property_type='Land'),',',' ') as is_land ,\
      array_to_string(array_agg(property_type='Mixed Use'),',',' ') as is_mixed_use ,\
      array_to_string(array_agg(property_type='Residential'),',',' ') as is_residential ,\
      array_to_string(array_agg(property_type='Special'),',',' ') as is_special ,\
      array_to_string(array_agg(property_type='Non-Real Estate'),',',' ') as is_non_real_estate ,\
      array_to_string(array_agg(property_type='Retail'),',',' ') as is_retail ,\
      array_to_string(array_agg(space_key_),',',' ') as sids ,\
      array_to_string(array_agg(property_type),',',' ') as stypes ,\
      array_to_string(array_agg(total_avail_sf),',',' ') as sfs ,\
      array_to_string(array_agg(suite),',',' ') as suits ,\
      min(total_avail_sf) as min_sf ,\
      max(total_avail_sf) as max_sf ,\
      min(rent_per_sf) as min_rent_per_sf ,\
      max(rent_per_sf) as max_rent_per_sf ,\
      min(COALESCE(rent_per_sf * total_avail_sf, 1)) as min_price ,\
      max(COALESCE(rent_per_sf * total_avail_sf, 1)) as max_price ,\
      array_to_string(array_agg(Rent_per_SF),',',' ') as rent_per_sfs ,\
      array_to_string(array_agg(COALESCE(rent_per_sf * total_avail_sf, 1)),',',' ') as prices ,\
      (array_agg(case when for_lease=True and for_sale=True then 'salelease' when for_sale=True then 'sale' when for_lease=True then 'lease' else 'other' end))[1] as offer_type, \
      (array_agg(for_lease))[1] as is_for_lease, \
      (array_agg(for_sale))[1] as is_for_sale \
      from " + cartoTableName + " where the_geom is not null group by pid",
  //,min(least(min_industrial,min_office,min_other,min_retail)) as min_price ,\
  sqlProperties = "select * from " + cartoTableName + " as a right join (" +
  sqlPropertiesAgg + ") as b on a.cartodb_id=b.id",
  //,sqlProperties = 'select * from ' + cartoTableName,
  sqlProperties = sqlProperties.replaceLS(currentLS),
  sqlZips ='SELECT zip_code as zip, (array_agg(city))[1] as city, (array_agg(state))[1] as state from ' +
    cartoTableName + ' group by zip_code',
    sqlCities = 'SELECT city as city, (array_agg(state))[1] as state from ' +
    cartoTableName + ' group by city',
  sqlByField = function(field) {
    return 'select ' + field + '_ as ' + field + ', count(' + field +
      '_) as num, avg(long) as lon, avg(lat) as lat \
              from ' +
      cartoTableName + ' group by ' + field + '_ order by num desc  ';
  },
  sqlPropType = function(propType) {
    return "select count(*) from " + cartoTableName + " where property_type='" +
      propType + "'";
  },
  sqlInfo = function(sid) {
    var sql =
      " select \
        address_1 as address_1 ,\
        city as city ,\
        state as state ,\
        zip_code as zip_code ,\
        property_name as name ,\
        property_type as type ,\
        property_type='Office' as is_office ,\
        property_type='Industrial' as is_industrial ,\
        property_type='Retail' as is_retail ,\
        property_type='Land' as is_land ,\
        property_type='Mixed Use' as is_mixed_use ,\
        property_type='Residential' as is_residential ,\
        property_type='Special' as is_special ,\
        property_type='Non-Real Estate' as is_non_real_estate ,\
        notes_  as notes ,\
        space_key_ as sid ,\
        suite as suit ,\
        Total_Avail_SF as sf ,\
        space_notes as snotes ,\
        loopnet_site as link ,\
        for_lease as is_for_lease ,\
        for_sale as is_for_sale ,\
        Rent_per_SF as rent_per_sf ,\
        price as price \
      from " +
      cartoTableName + " where space_key_='" + sid + "'";
    return sql;
  },
  popupFields = "address,name,type,sids,stypes,suits,sfs,link,is_retail,is_office,is_industrial,is_for_lease,is_for_sale,offer_type,notes_",

  makeSQL = function(state) {
    //console.log(this, state, _keys(state).length, _keys(state, 1).length, _keys(state));
    console.log(state);

    map.data.forEach(function(d,i){
      var show = true;

      if (state.lease || state.sale || state.liquorlicense || state.other)
        show = (state.lease && d.is_for_lease) || (state.sale && d.is_for_sale) || (state.other && !d.is_for_lease && !d.is_for_sale);

      if (show && state.price_min)
        show = (!d.min_price || +d.min_price > +state.price_min) || (!d.min_rent_per_sf || +d.min_rent_per_sf > +state.price_min);
      if (show && state.price_max)
        show = (!d.max_price || +d.max_price < +state.price_max) || (!d.max_rent_per_sf || +d.max_rent_per_sf < +state.price_max);

      if (show && state.size_min)
        show = !d.total_avail_sf || +d.total_avail_sf > +state.size_min;
      if (show && state.size_max)
        show = !d.total_avail_sf || +d.total_avail_sf < +state.size_max;

      if (show && state.property_type) {
        show = false;
        for (var field in state.property_type) {
          if (d.property_type == field) show = true;
          if ((field == 'Commercial' || field == 'Retail') && (d.sale___retail || d.lease___retail || d.sale_or_lease___retail)) show = true;
          if ((field == 'Office') && (d.sale___office || d.lease___office || d.sale_or_lease___office)) show = true;
          if ((field == 'Industrial') && (d.sale___industrial || d.lease___industrial || d.sale_or_lease___industrial)) show = true;
          if ((field == 'Land') && (d.sale___land || d.lease___land || d.sale_or_lease___land)) show = true;
          if ((field == 'Mixed Use') && (d.sale___mixed_use || d.lease___mixed_use || d.sale_or_lease___mixed_use)) show = true;
          if ((field == 'Residential') && (d.sale___residential || d.lease___residential || d.sale_or_lease___residential)) show = true;
          if ((field == 'Special') && (d.sale___special || d.lease___special || d.sale_or_lease___special)) show = true;
          if ((field == 'Non Real Estate') && (d.sale___non_real_estate || d.lease___non_real_estate || d.sale_or_lease___non_real_estate)) show = true;
        }
      }

      if (show && state.keyword_input) {
        show = ((d.name||'')+(d.property_name_||'')+(d.notes||'')+(d.notes_||'')+(d.address||'')+(d.building_class||'')+(d.regional_market||'')).toLowerCase().indexOf(state.keyword_input.toLowerCase()) != -1;
      }

      if (show && state.state) {
        show = d.state_ && d.state_.toLowerCase().indexOf(state.state.toLowerCase()) != -1;
      }
      if (show && state.city) {
        show = d.city_ && d.city_.toLowerCase().indexOf(state.city.toLowerCase()) != -1;
      }
      if (show && state.zip_code) {
        show = d.zip_code_ && d.zip_code_.indexOf(state.zip_code) != -1;
      }
      if (show && state.circle){
        show = d.latlng.distanceTo(state.circle.latlng) < state.circle.radius;
      }
      if (show && state.bounds){
        show = state.bounds.contains(d.latlng);
      }

      d.hide = !show;

    })


    map.filterMarkers();

    var fData = map.data.filter(function(d){ return !d.hide;});
    searchResults(fData);

  };

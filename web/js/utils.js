/*jshint -W030 */
/*jshint -W004 */

function streetImage(selector, lat, lng) {
	var url = googleStreetImageUrl + lat + ',' + lng;
	$(selector).parent().hide();
	$('#loadingImage').parent().show();
	$(selector).attr('src', url).on('load', function() {
		$(this).parent().show();
		$('#loadingImage').parent().hide();
	});
	$('#streetLink').click(function() {
		console.log(this);
		window.open(googleStreetUrl + lat + ',' + lng);
	});
}
var lastImageUrl;

var addCursorInteraction = function(layer) {
	var hovers = [];

	layer.bind('featureOver', function(e, latlon, pxPos, data, layer) {
		hovers[layer] = 1;
		if (_.any(hovers)) {
			$('#map').css('cursor', 'pointer');
		}
	});

	layer.bind('featureOut', function(m, layer) {
		hovers[layer] = 0;
		if (!_.any(hovers)) {
			$('#map').css('cursor', 'auto');
		}
	});
};

function _keys(object, ifTrue) {
	var arr = [];
	for (var field in object)
		(!ifTrue || object[field]) && arr.push(field);
	return arr;
}

String.prototype.formatFieldsLikeCartodb = function() {
	return this.replace(/[- ]/g, '_').toLowerCase();
};
String.prototype.replaceLS = function(currentLS) {
	return this.replace(/\{\{leaseOrSale\}\}/g, leaseOrSale[currentLS]);
};
String.prototype.formatNotes = function(length) {
	var formatted = this.replace(/\{.+\\fs../g, '').replace(/\}/g, '').substring(0, length).match(/.+ /g);
	return formatted ? formatted + '...' : null;
};
Number.prototype.formatMoney = function(c, d, t) {
	var n = this,
		c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d === undefined ? "." : d,
		t = t === undefined ? "," : t,
		s = n < 0 ? "-" : "",
		i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
		j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(
		/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) :
		"");
};

L.GeometryUtil.readableDistance = function(distance, isMetric) {
	var distanceStr;

	if (isMetric) {
		// show metres when distance is < 1km, then show km
		if (distance > 1000) {
			distanceStr = (distance / 1000).toFixed(2) + ' km';
		} else {
			distanceStr = Math.ceil(distance) + ' m';
		}
	} else {
		distance *= 1.09361;

		if (distance > 1760) {
			distanceStr = (distance / 1760).toFixed(1) + ' miles';
		} else {
			distanceStr = (distance / 1760).toFixed(2) + ' miles';
		}
	}

	return distanceStr;
};

L.GeometryUtil.readableArea = function(area, isMetric) {
	var areaStr;

	if (isMetric) {
		if (area >= 10000) {
			areaStr = (area * 0.0001).toFixed(2) + ' ha';
		} else {
			areaStr = area.toFixed(2) + ' m&sup2;';
		}
	} else {
		area /= 0.836127; // Square yards in 1 meter

		if (area >= 3097600) { //3097600 square yards in 1 square mile
			areaStr = (area / 3097600).toFixed(1) + ' mi&sup2;';
		} else if (area >= 4840) { //48040 square yards in 1 acre
			areaStr = (area / 4840).toFixed(0) + ' acres';
		} else {
			areaStr = (area / 4840).toFixed(2) + ' acres';
		}
	}

	return areaStr;
};

//http://stackoverflow.com/questions/1740700/how-to-get-hex-color-value-rather-than-rgb-value
function rgb2hex(rgb) {
    if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;

    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}


var mapControlSwitch = L.Control.extend({
		initialize: function(foo, options){
			console.log(foo,options)
			L.Util.setOptions(this, options);
		},
    onAdd: function (map) {
        // create the control container with a particular class name
        var container = L.DomUtil.create('div', 'switch-map-control leaflet-bar leaflet-control');

				console.log(this.options);

        $(container).html('<span class="no-mobile">Switch Map</span><span class="yes-mobile">☰</span>');

				$(container).click(this.options.switch);

        return container;
    }
});

var addDrawControl = function(map){
  var makeSQL = map.makeSQL,
    state = map.state,
    boundsLayer = new L.FeatureGroup();

  map.addLayer(boundsLayer);

  map.drawControl = new L.Control.Draw({
    position: 'topright',
    polyline: false,
    polygon: false,
    marker: false,
    circle: false,
    metric: false,
    edit: {
      featureGroup: boundsLayer,
      edit: true
    }
  });
  //console.log(map.drawControl._toolbars.draw.options.circle, map.drawControl)

  var filterByDraw = function(layer, type){
    state = map.state;
    console.log('filterByDraw', layer, type, state);
    delete state.circle;
    delete state.bounds;
    if (type === 'circle') {
      state.circle = {
        latlng: layer._latlng,
        radius: layer._mRadius
      };
    }
    if (type === 'rectangle') {
      state.bounds = layer.getBounds();
    }
    map.makeSQL(state);
  }
  map.addControl(map.drawControl)
    .on('draw:created', function(e) {
      console.log(e.layerType, e);
      //console.log(e.layer.path, e.layer.path.getBounds());
      var type = e.layerType,
        layer = e.layer;

      map.lastBoundsLayer && map.removeLayer(map.lastBoundsLayer);
      //map.addLayer(layer);
      map.lastBoundsLayer = layer;
      boundsLayer.addLayer(layer);
      console.log(boundsLayer);
      layer.editing.enable();

      layer.on('edit',function(e){
        filterByDraw(e.target, type);
      });

      filterByDraw(layer, type);
    });
}
